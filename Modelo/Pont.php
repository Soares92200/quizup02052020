<?php
    class Pont{
        private $id;
        private $idUsuario;
        private $ponto;
        private $idArea;
        private $verdFal;
        
        public function getId(){
            return $this->id;
        }
        public function setId($i){
            $this->id = $i;
        }
        public function getIdUsuario(){
            return $this->idUsuario;
        }
        public function setIdUsuario($iu){
            $this->idUsuario = $iu;
        }
        public function getPonto(){
            return $this->ponto;
        }
        public function setPonto($p){
            $this->ponto = $p;
        }
        public function getIdArea(){
            return $this->idArea;
        }
        public function setIdArea($ia){
            $this->idArea = $ia;
        }
        public function getVerdFal(){
            return $this->verdFal;
        }
        public function setVerdFal($vf){
            $this->verdFal = $vf;
        }

    }
?>  