<?php
session_start();

$resp = $_SESSION['resposta'];
unset($_SESSION['resposta']);

$ia = $_SESSION['idArea'];

$ia = (int)$ia;
unset($_SESSION['idArea']);

$id = $_SESSION['IdUser'];

$idUser = (int)$id["idUser"];

require_once("Controle.php");
require_once("../Modelo/Pont.php");

try{
    $control = new ControleUsuario();

    $p = new Pont();

    $pont = $control->selecionarPid($idUser);

    $p->setIdUsuario($idUser);
    $p->setIdArea($ia);

    if($_GET['res'] == $resp){
        $pont = (int)$pont['pontuacao']+10;
        $pont = "$pont";
        $vf = 'V';
        $p->setPonto($pont);
    }else{
        $vf = 'F';
        if($pont =='0'){
            header("Location: ../Visual/Principal.php");
        }else{
            $pont = (int)$pont['pontuacao']-10;
            $pont = "$pont";
            $p->setPonto($pont);
        }
    }
    $p->setVerdFal($vf);
    $control->pontuacao($p);
    header("Location: ../Visual/Jogo.php");
}catch(Exception $e){
    echo "Erro: $e->getMessage()";
}

?>